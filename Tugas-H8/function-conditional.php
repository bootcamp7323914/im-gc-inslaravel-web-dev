!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>

<body>
    <h1>Berlatih Function PHP</h1>
    <?php
    echo "<h3>Soal No 1 Greetings</h3>";

    function greetings($name)
    {
        $greeting = "Halo " . ucfirst($name) . ", Selamat Datang di Sanbercode!";
        echo $greeting;
    }

    greetings("abduh"); // contoh pemanggilan function
    echo "<h3>Soal No 2 Reverse String</h3>";

    function reverseString($string)
    {
        $length = strlen($string);
        $result = "";

        for ($i = $length - 1; $i >= 0; $i--) {
            $result .= $string[$i];
        }

        echo $result;
    }

    reverseString("abdul");
    echo "<br>";
    reverseString("nama peserta");
    echo "<br>";
    reverseString("Sanbercode");
    echo "<br>";
    reverseString("We Are Sanbers Developers");
    echo "<h3>Soal No 3 Palindrome </h3>";

    function palindrome($string)
    {
        $reversedString = reverseString($string); // call function from answer to no. 2
        if ($string === $reversedString) {
            return true;
        } else {
            return false;
        }
    }

    // Test the function with some input strings
    echo "palindrome('civic') : " . (palindrome('civic') ? 'true' : 'false') . "<br>"; // true
    echo "palindrome('nababan') : " . (palindrome('nababan') ? 'true' : 'false') . "<br>"; // true
    echo "palindrome('jambaban') : " . (palindrome('jambaban') ? 'true' : 'false') . "<br>"; // false
    echo "palindrome('racecar') : " . (palindrome('racecar') ? 'true' : 'false') . "<br>"; // true
    echo "<h3>Soal No 4 Tentukan Nilai </h3>";

    function tentukan_nilai($nilai)
    {
        if ($nilai >= 85 && $nilai <= 100) {
            return "Sangat Baik";
        } elseif ($nilai >= 70 && $nilai < 85) {
            return "Baik";
        } elseif ($nilai >= 60 && $nilai < 70) {
            return "Cukup";
        } else {
            return "Kurang";
        }
    }

    echo tentukan_nilai(98); // Sangat Baik
    echo "<br>";
    echo tentukan_nilai(76); // Baik
    echo "<br>";
    echo tentukan_nilai(67); // Cukup
    echo "<br>";
    echo tentukan_nilai(43); // Kurang
    ?>
</body>

</html>
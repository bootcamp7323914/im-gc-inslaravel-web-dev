<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>

<body>
    <h1>Berlatih String PHP</h1>
    <?php
    echo "<h3> Soal No 1</h3>";
    /* SOAL NO 1 
    Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! 
    Tunjukkan juga jumlah kata di dalam kalimat tersebut!
    Contoh: $string = "PHP is never old"; Output: Panjang string: 16, Jumlah kata: 4
    */
    $first_sentence = "Hello PHP!";
    $second_sentence = "I'm ready for the challenges";
    echo "Kalimat pertama: \"$first_sentence\" <br>";
    $first_sentence_length = strlen($first_sentence);
    $first_sentence_word_count = str_word_count($first_sentence);
    echo "Panjang string: $first_sentence_length, Jumlah kata: $first_sentence_word_count <br><br>";

    echo "Kalimat kedua: \"$second_sentence\" <br>";
    $second_sentence_length = strlen($second_sentence);
    $second_sentence_word_count = str_word_count($second_sentence);
    echo "Panjang string: $second_sentence_length, Jumlah kata: $second_sentence_word_count";

    echo "<h3> Soal No 2</h3>";
    /* SOAL NO 2 
Mengambil kata pada string dan karakter-karakter yang ada di dalamnya.
*/
    $string2 = "I love PHP";
    echo "<label>String: </label> \"$string2\" <br>";
    echo "Kata pertama: " . substr($string2, 0, 1) . "<br>";
    echo "Kata kedua: " . substr($string2, 2, 4) . "<br>";
    echo "Kata Ketiga: " . substr($string2, 7, 3) . "<br>";

    echo "<h3> Soal No 3 </h3>";
    /* SOAL NO 3 
Mengubah karakter atau kata yang ada di dalam sebuah string.
*/
    $string3 = "PHP is old but sexy!";
    echo "String: \"$string3\" <br>";
    $new_string3 = str_replace("sexy", "awesome", $string3);
    echo "String baru: \"$new_string3\"";
    ?>
</body>

</html>
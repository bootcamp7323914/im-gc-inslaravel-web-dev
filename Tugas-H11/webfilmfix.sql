﻿# Host: localhost  (Version 5.5.5-10.4.24-MariaDB)
# Date: 2023-04-11 00:40:43
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "cast"
#

DROP TABLE IF EXISTS `cast`;
CREATE TABLE `cast` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `umur` int(11) DEFAULT NULL,
  `bio` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "cast"
#

INSERT INTO `cast` VALUES (1,'Brad Pitt',57,'Brad Pitt adalah seorang aktor dan produser film Amerika Serikat.'),(2,'Emma Stone',32,'Emma Stone adalah seorang aktris Amerika Serikat.'),(3,'Tom Cruise',59,'Tom Cruise adalah seorang aktor dan produser film Amerika Serikat.');

#
# Structure for table "film"
#

DROP TABLE IF EXISTS `film`;
CREATE TABLE `film` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `ringkasan` text DEFAULT NULL,
  `tahun` int(11) DEFAULT NULL,
  `poster` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "film"
#

INSERT INTO `film` VALUES (1,'Avengers: Endgame','Avengers kembali untuk menghadapi Thanos',2019,'https://example.com/poster1.jpg'),(2,'Joker','Kisah asal-usul Joker',2019,'https://example.com/poster2.jpg'),(3,'Parasite','Keluarga pengangguran berencana merampok keluarga kaya',2019,'https://example.com/poster3.jpg');

#
# Structure for table "genre"
#

DROP TABLE IF EXISTS `genre`;
CREATE TABLE `genre` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "genre"
#

INSERT INTO `genre` VALUES (1,'Action'),(2,'Comedy'),(3,'Drama');

#
# Structure for table "profil"
#

DROP TABLE IF EXISTS `profil`;
CREATE TABLE `profil` (
  `id` int(11) NOT NULL,
  `umur` int(11) DEFAULT NULL,
  `bio` text DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "profil"
#

INSERT INTO `profil` VALUES (1,20,'John','Jl. Raya 1'),(2,25,'Jane','Jl. Raya 2'),(3,30,'Jim','Jl. Raya 3');

#
# Structure for table "user"
#

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

#
# Data for table "user"
#

INSERT INTO `user` VALUES (1,'john_doe','john@example.com','password1'),(2,'jane_doe','jane@example.com','password2'),(3,'jim_smith','jim@example.com','password3');
